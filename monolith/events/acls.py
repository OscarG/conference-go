import requests
import json
from .keys import PEXEL_API_KEYS, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    request_url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {"Authorization": PEXEL_API_KEYS}
    response = requests.get(request_url, headers=headers)
    picture = json.loads(response.content)
    picture_url = {"picture_url": picture["photos"][0]["url"]}
    return picture_url

    # params = {
    #     "per_page": 1,
    #     "query": city + "," + state
    # }
    # request.get(url, params=params, headers=headers)


def get_weather(city, state):
    request_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(request_url, params=params)
    content = json.loads(response.content)
    try:
        location_lat = content[0]["lat"]
        location_lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": location_lat,
        "lon": location_lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "temperature": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
